const Encore = require('@symfony/webpack-encore');

Encore
	.setOutputPath('dist/javascript/')
	.setPublicPath('/javascript/')
	.addEntry('index', './src/javascript/index.js')
	.enableSingleRuntimeChunk()
	.enableSourceMaps(!Encore.isProduction())
	.configureBabel(function(bc) {
		bc.plugins.push('@babel/plugin-syntax-jsx');
		bc.plugins.push(['@babel/plugin-transform-react-jsx', {"pragma": "dom"}]);
	}, { });
if(Encore.isProduction()) {
	Encore
		.cleanupOutputBeforeBuild()
		.enableVersioning()
	;
}
module.exports = Encore.getWebpackConfig();
