module.exports = function(eleventyConfig) {
	/* Pass through - stop eleventy touching */
	eleventyConfig.addPassthroughCopy('src/images');
	eleventyConfig.addPassthroughCopy({'netlify-cms/admin': 'admin'});
	eleventyConfig.addPassthroughCopy({'node_modules/98.css/dist/98.css': 'styles/98.css'});
	eleventyConfig.addPassthroughCopy({'node_modules/98.css/dist/98.css.map': 'styles/98.css.map'});
	eleventyConfig.addPassthroughCopy({'node_modules/98.css/dist/icon.png': 'styles/icon.png'});
	eleventyConfig.addPassthroughCopy({'node_modules/98.css/dist/ms_sans_serif_bold.woff': 'styles/ms_sans_serif_bold.woff'});
	eleventyConfig.addPassthroughCopy({'node_modules/98.css/dist/ms_sans_serif_bold.woff2': 'styles/ms_sans_serif_bold.woff2'});
	eleventyConfig.addPassthroughCopy({'node_modules/98.css/dist/ms_sans_serif.woff': 'styles/ms_sans_serif.woff'});
	eleventyConfig.addPassthroughCopy({'node_modules/98.css/dist/ms_sans_serif.woff2': 'styles/ms_sans_serif.woff2'});
	eleventyConfig.addCollection("authors", collection => {
		const posts = collection.getFilteredByGlob("src/posts/*.md");
		return posts.reduce((coll, post) => {
			const author = post.data.author;
			if(!author) {
				return coll;
			}
			if(!coll.hasOwnProperty(author)) {
				coll[author] = [];
			}
			coll[author].push(post);
			return coll;
		}, {});
	});
	/* Stolen from https://www.11ty.dev/docs/languages/markdown/ */
	let markdownIt = require("markdown-it");
	let markdownItAttrs = require("markdown-it-attrs");
	let markdownItDiv = require("markdown-it-div");
	let markdownItFootnote = require("markdown-it-footnote");
	let cryptoStr = require('crypto-random-string');
	let htmlmin = require("html-minifier");
	let strftime = require("strftime");
	let options = {
		html: true,
		typographer: true
	};
	let markdownLib = markdownIt(options).use(markdownItFootnote).use(markdownItAttrs).use(markdownItDiv);
	markdownLib.renderer.rules.footnote_anchor_name = function(tokens, idx, options, env) {
		var n = Number(tokens[idx].meta.id + 1).toString();
		if(!env.docId) {
			env.docId = cryptoStr({length: 10, type: 'url-safe'});
		}
		var prefix = '';
		if(typeof env.docId === 'string') {
			prefix = '-' + env.docId + '-';
		}
		return prefix + n;
	}
	eleventyConfig.setLibrary("md", markdownLib);
	eleventyConfig.addTransform("htmlmin", function(content, outputPath) {
		if(typeof outputPath === 'string' && outputPath.endsWith(".html")) {
			let minified = htmlmin.minify(content, {
				useShortDoctype: true,
				removeComments: true,
				collapseWhitespace: true
			});
			return minified;
		}
		return content;
	});
	eleventyConfig.addFilter("strftime", function(input, format) {
		return strftime(format, input);
	});
	eleventyConfig.addPairedShortcode("markdown", (content) => {
		return markdownLib.render(content);
	});
	return {
		dir: { input: 'src', output: 'dist', data: '_data' },
		passthroughFileCopy: true,
		templateFormats: ['njk', 'md', 'css', 'html', 'yml', '11ty.js'],
		htmlTemplateEngine: 'njk'
	};
}
