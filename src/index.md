---
layout: layouts/home.njk
title: Kpop with Kaori
eleventyExcludeFromCollections: true
---
::: text-center
[![Animated GIF of GOT7](images/header-got7.gif)](/tags/got7/)
[![Animated GIF of IU](images/header-iu.gif)](/tags/iu/)
[![Animated GIF of NCT](images/header-nct.gif)](/tags/nct/)
[![Animated GIF of Blackpink](images/header-blackpink.gif)](/tags/blackpink/)
[![Animated GIF of Mamamoo](images/header-mamamoo.gif)](/tags/mamamoo/)
[![Animated GIF of EXO](images/header-exo.gif)](/tags/exo/)
[![Animated GIF of TXT](images/header-txt.gif)](/tags/txt/)
[![Animated GIF of Twice](images/header-twice.gif)](/tags/twice/)
[![Animated GIF of BTS](images/header-bts.gif)](/tags/bts/)
[![Animated GIF of SHINee](images/header-shinee.gif)](/tags/shinee/)
:::
