import "core-js/stable";
import "regenerator-runtime/runtime";
import Fuse from "fuse.js";
import dom from "jsx-render";
const options = {
	keys: ["name"],
	minlength: 3,
	maxresults: 5,
	updatetime: 1000
};
const processFuncs = {
	"Author": function(item) {
		return (<a href={item.permalink}><li class="search-result result-author">
			<span>Author</span>
			<span>{item.name}</span>
		</li></a>);
	},
	"Post": function(item) {
		return (<a href={item.permalink}><li class="search-result result-post">
			<span>Post</span>
			<span>{item.name}</span>
		</li></a>);
	},
	"Tag": function(item) {
		return (<a href={item.permalink}><li class="search-result result-tag">
			<span>Tag</span>
			<span>{item.name}</span>
		</li></a>);
	}
};
fetch("/documents.json")
	.then(response => response.json())
	.then(function(documents) {
		const fuse = new Fuse(documents, options);
		const search_div = document.getElementById('search');
		const search_input = document.getElementById('search-input');
		const search_results = document.getElementById('search-results');
		const change_func = function(evt) {
			const inlen = search_input.value.length;
			if(inlen < options.minlength) {
				search_results.innerHTML = '';
				search_results.style.display = 'none';
				return;
			}
			const results = fuse.search(search_input.value);
			if(results.length > options.maxresults) {
				results.length = options.maxresults;
			}
			search_results.innerHTML = '';
			if(results.length === 0) {
				search_results.style.display = 'none';
			} else {
				for(const result of results) {
					const cat = result.item.category;
					if(!(cat in processFuncs)) {
						continue;
					}
					const res = processFuncs[cat](result.item);
					search_results.appendChild(res);
				}
				search_results.style.display = 'block';
				search_results.focus();
				search_results.click();
			}
		}
		search_div.style.display = 'block';
		for(const evt of ['keyup', 'change']) {
			search_input.addEventListener(evt, change_func);
		}
	});
