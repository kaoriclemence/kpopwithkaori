const slugify = require('slugify');
const striptags = require('striptags');
const slopts = {
	lower: true
};
class Documents {
	data() {
		return {
			permalink: "documents.json",
			eleventyExcludeFromCollections: true
		};
	}
	render({collections}) {
		let out = new Set();
		for(const post of collections.all) {
			if(post.data.title) {
				out.add(JSON.stringify({
					"category": "Post",
					"name": post.data.title,
					"content": striptags(post.templateContent).replace(/\r?\n|\r/g, " "),
					"permalink": post.url,
				}));
			}
			if(post.data.author) {
				out.add(JSON.stringify({
					"category": "Author",
					"name": post.data.author,
					"permalink": "/authors/" + slugify(post.data.author, slopts) + "/",
				}));
			}
			if(post.data.tags) {
				for(const tag of post.data.tags) {
					out.add(JSON.stringify({
						"category": "Tag",
						"name": tag,
						"permalink": "/tags/" + slugify(tag, slopts) + "/",
					}));
				}
			}
		}
		const items = Array.from(out).map(x => JSON.parse(x));
		return JSON.stringify(items);
	}
}
module.exports = Documents;
